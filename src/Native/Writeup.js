var make = function make(elm) {
  elm.Native = elm.Native || {};
  elm.Native.Writeup = elm.Native.Writeup || {};

  if (elm.Native.Writeup.values) {
    return elm.Native.Writeup.values;
  }

  var execCommand = function(command, valueArgument) {
    document.execCommand(command, false, valueArgument);
  };

  var getSelection = function(_bogus) {
    var node = document.getSelection().anchorNode;

    if (node === null) {
      return null;
    }

    var nodeHtml = node.nodeType === 3 ? node.parentNode : node;

    return nodeHtml.tagName;
  };

  return elm.Native.Writeup.values = {
    "execCommand": F2(execCommand),
    "getSelection": getSelection
  };
};

Elm.Native.Writeup = {};
Elm.Native.Writeup.make = make;
