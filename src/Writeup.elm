module Writeup (BlockStyle(..), Action(..), Editor, view, update, withControls, initialEditor) where

{-| This library is a test library for a simple markdown wysiwyg editor.

# Definition
@docs BlockStyle, Action, Editor, withControls, initialEditor

# State
@docs update

# Views
@docs view

-}

import Writeup.Css exposing (writeupNamespace, inlineCssFor)
import Writeup.Events as Events
import Native.Writeup

import Html exposing (Html, div, text, span, p, a)
import Html.Attributes exposing (contenteditable, attribute, href)
import Json.Encode as JE
import String

{-| The Editor for keeping state of content. -}
type alias Editor
  = { controls : List BlockStyle
    , content : List ContentBlock
    , rawContent : String
    , htmlContent : String
    , activeControl : BlockStyle
    }

type alias ContentBlock
  = { style : BlockStyle
    , content : String
    }

type alias TagName = String

{-| The Actions we listen to for updating the internal state of the Editor. -}
type Action
  = StyleSelected BlockStyle
  | ContentClicked TagName
  | UpdatedRawContent String
  | UpdatedHtmlContent String
  | NoOp

{-| The possible styles a block can have in the editor. -}
type BlockStyle
  = HeadingOne
  | HeadingTwo
  | HeadingThree
  | HeadingFour
  | HeadingFive
  | HeadingSix
  | Quote
  | UnorderedList
  | OrderedList
  | Bold
  | Paragraph

{ id, class, classList } = writeupNamespace

{-| Initial default editor styles -}
initialEditor : Editor
initialEditor =
  { controls =
      [ HeadingOne
      , HeadingTwo
      , Quote
      , UnorderedList
      , OrderedList
      , Bold
      ]
  , content = []
  , rawContent = ""
  , htmlContent = ""
  , activeControl = Paragraph
  }

{-| This is the javascript implementation for editing
`contenteditable` elements in HTML. Using `execCommand` will
allow you to add element styles to the current cursor position. -}
execCommand : String -> JE.Value -> ()
execCommand = Native.Writeup.execCommand

{-| Setup the controls bar in your editor. -}
withControls : List BlockStyle -> Editor -> Editor
withControls controls editor =
  { editor | controls = controls}

html5Command : BlockStyle -> (String, JE.Value)
html5Command style =
  case style of
    HeadingOne -> ("formatBlock", JE.string "h1")
    HeadingTwo -> ("formatBlock", JE.string "h2")
    HeadingThree -> ("formatBlock", JE.string "h3")
    HeadingFour -> ("formatBlock", JE.string "h4")
    HeadingFive -> ("formatBlock", JE.string "h5")
    HeadingSix -> ("formatBlock", JE.string "h6")
    Quote -> ("formatBlock", JE.string "blockquote")
    UnorderedList -> ("insertUnorderedList", JE.null)
    OrderedList -> ("insertOrderedList", JE.null)
    Bold -> ("bold", JE.null)
    Paragraph -> ("formatBlock", JE.string "p")

{-| nodoc -}
update : Action -> Editor -> Editor
update action editor =
  case action of
    StyleSelected style ->
      let
        (command, valueArg) = html5Command style
        _ = execCommand command valueArg
      in
        { editor | activeControl = style }

    ContentClicked tagName ->
      let
        style = tagNameToBlockStyle tagName
        (command, valueArg) = html5Command style
        _ = execCommand command valueArg
      in
        { editor | activeControl = style }
    UpdatedRawContent raw ->
      { editor | rawContent = raw }
    UpdatedHtmlContent html' ->
      { editor | htmlContent = html' }
    NoOp ->
      editor

{-| Dummy test -}
view : Signal.Address Action -> Editor -> Html
view address editor =
  div
    ( [ class [ Writeup.Css.Root ] ]
    ++ inlineCssFor [ Writeup.Css.Root ]
    )
    [ div
        ( [ class [Writeup.Css.Controls ] ]
        ++ inlineCssFor [ Writeup.Css.Controls ]
        )
        [ viewControls address editor ]
    , div
        ( [ class [ Writeup.Css.Editor ] ]
        ++ inlineCssFor [ Writeup.Css.Editor ]
        )
        [ viewEditorArea address editor ]
    ]

viewEditorArea : Signal.Address Action -> Editor -> Html
viewEditorArea address editor =
  div
    ( [ class [ Writeup.Css.EditorRoot ] ]
    ++ inlineCssFor [ Writeup.Css.EditorRoot ]
    )
    [ div
        ( [ class [ Writeup.Css.EditorContainer ] ]
        ++ inlineCssFor [ Writeup.Css.EditorContainer ]
        )
        [ viewEditorContent address editor ]
    ]

viewEditorContent : Signal.Address Action -> Editor -> Html
viewEditorContent address editor =
  div
    ( [ class [ Writeup.Css.EditorContent ]
      , contenteditable True
      , Events.onHtmlInput address UpdatedHtmlContent
      , Events.onTextInput address UpdatedRawContent
      , Events.onFocusFirst address (StyleSelected Paragraph) NoOp
      , Events.onEnter address (StyleSelected editor.activeControl)
      , Events.onFocusSelection address ContentClicked
      ]
        ++ inlineCssFor [ Writeup.Css.EditorContent ]
    )
    []

renderStyleBlocks : Signal.Address Action -> Editor -> List Html
renderStyleBlocks address editor =
  List.map (\c -> renderStyleBlock c editor) editor.content

renderStyleBlock : ContentBlock -> editor -> Html
renderStyleBlock content editor =
  case content.style of
    _ -> div [] []

{-| private -}
viewControls : Signal.Address Action -> Editor -> Html
viewControls address editor =
  div
    ( [ class [ Writeup.Css.Controls ] ]
    ++ inlineCssFor [ Writeup.Css.Controls ]
    )
    (List.map (\c -> viewControl address c editor.activeControl) editor.controls)

{-| private -}
viewControl : Signal.Address Action -> BlockStyle -> BlockStyle -> Html
viewControl address style activeStyle =
  let
    classes =
      [ Writeup.Css.StyleButton ]
    (classes', nextStyle) = if activeStyle == style then
                 ( classes ++ [ Writeup.Css.ActiveStyleButton ]
                 , Paragraph
                 )
               else
                 ( classes
                 , style
                 )
  in
    a
      ( [ class classes'
        , href "#"
        , Events.onClickControl address (StyleSelected nextStyle)
        ]
      ++ ( inlineCssFor classes' )
      )
      [ text <| styleToString style ]

{-| private -}
styleToString : BlockStyle -> String
styleToString style =
  case style of
    HeadingOne -> "H1"
    HeadingTwo -> "H2"
    HeadingThree -> "H3"
    HeadingFour -> "H4"
    HeadingFive -> "H5"
    HeadingSix -> "H6"
    Quote -> "Blockquote"
    UnorderedList -> "UL"
    OrderedList -> "OL"
    Bold -> "Bold"
    Paragraph -> "Text"

{-| private -}
tagNameToBlockStyle : String -> BlockStyle
tagNameToBlockStyle tagName =
  case (String.toLower tagName) of
    "h1" -> HeadingOne
    "h2" -> HeadingTwo
    "h3" -> HeadingThree
    "h4" -> HeadingFour
    "h5" -> HeadingFive
    "h6" -> HeadingSix
    "blockquote" -> Quote
    "ul" -> UnorderedList
    "ol" -> OrderedList
    "b" -> Bold
    "strong" -> Bold
    _ -> Paragraph
