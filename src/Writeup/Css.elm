module Writeup.Css(CssClasses(..), css, writeupNamespace, inlineCssFor) where

import Css exposing (..)
import Css.Namespace exposing (namespace)
import Html
import Html.CssHelpers exposing (withNamespace)
import Html.CssHelpers
import Html.Attributes

type CssClasses
  = Root
  | Controls
  | StyleButton
  | ActiveStyleButton
  | Editor
  | EditorRoot
  | EditorContainer
  | EditorContent

writeupNamespace : Html.CssHelpers.Namespace String a b
writeupNamespace =
  withNamespace "Writeup"

css : Stylesheet
css =
  (stylesheet << namespace writeupNamespace.name)
    [ ((.) Root)
        (cssFor Root)
    , ((.) Controls)
        (cssFor Controls)
    , ((.) Editor)
        (cssFor Editor)
    , ((.) StyleButton)
        (cssFor StyleButton)
    ]

styles : List Mixin -> Html.Attribute
styles = Css.asPairs >> Html.Attributes.style

inlineCssFor : List CssClasses -> List Html.Attribute
inlineCssFor = List.map (styles << cssFor)


cssFor : CssClasses -> List Mixin
cssFor klass =
  case klass of
    Root ->
      [ backgroundColor (hex "#fff")
      , border3 (px 1) solid (hex "#ddd")
      , fontFamilies [ "Georgia", "serif" ]
      , fontSize (px 14)
      , padding (px 15)
      ]
    Controls ->
      [ fontFamilies [ "Helvetica", "sans-serif" ]
      , fontSize (px 14)
      , marginBottom (px 5)
      ]
    StyleButton ->
      [ color (hex "#999")
      , property "cursor" "pointer"
      , marginRight (px 16)
      , padding2 (px 2) (pt 0)
      , display inlineBlock
      ]
    ActiveStyleButton ->
      ( cssFor StyleButton ) ++ [ color (hex "#5890ff") ]
    Editor ->
      [ borderTop3 (px 1) solid (hex "#ddd")
      , property "cursor" "text"
      , fontSize (px 16)
      , marginTop (px 10)
      ]
    EditorRoot ->
      [ position relative ] ++ sharedEditorStyle
    EditorContainer ->
      [ backgroundColor (rgb 255 255 255)
      , borderLeft3 (px 0.1) solid transparent
      , position relative
      , property "z-index" "1"
      ] ++ sharedEditorStyle
    EditorContent ->
      [ property "outline" "none"
      , property "white-space" "pre-wrap"
      , property "wordWrap" "breakWord"
      , minHeight (px 100)
      , margin3 (pt 0) (px -15) (px -15)
      , padding (px 15)
      ] ++ sharedEditorStyle

sharedEditorStyle : List Mixin
sharedEditorStyle =
  [ height inherit
  , property "text-align" "inherit"
  ]
