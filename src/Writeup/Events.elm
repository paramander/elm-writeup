module Writeup.Events where

import Html exposing (Html, div, text, span, p, a)
import Html.Events exposing (onClick, on, targetValue, onWithOptions, keyCode)
import Json.Decode as JD
import String

import Native.Writeup

targetTextContent : JD.Decoder String
targetTextContent =
  JD.at ["target", "textContent"] JD.string

targetHtmlContent : JD.Decoder String
targetHtmlContent =
  JD.at ["target", "innerHTML"] JD.string

onHtmlInput : Signal.Address a -> (String -> a) -> Html.Attribute
onHtmlInput address action =
  on
    "input"
    targetHtmlContent
    (\str -> Signal.message address (action str))

onTextInput : Signal.Address a -> (String -> a) -> Html.Attribute
onTextInput address action =
  on
    "input"
    targetTextContent
    (\str -> Signal.message address (action str))

onFocusFirst : Signal.Address a -> a -> a -> Html.Attribute
onFocusFirst address emptyAct filledAct =
  on
    "focus"
    targetTextContent
    (\str ->
       let act = if String.isEmpty str then
                   emptyAct
                 else
                   filledAct
       in Signal.message address act
    )

isEnter : Int -> Result String ()
isEnter code =
  if code == 13 then Ok () else Err "not enter"

onEnter : Signal.Address a -> a -> Html.Attribute
onEnter address act =
  on
    "keypress"
    (JD.customDecoder keyCode isEnter)
    (\_ -> Signal.message address act)

getSelection : Maybe String -> String
getSelection arg =
  Native.Writeup.getSelection arg

onFocusSelection : Signal.Address a -> (String -> a) -> Html.Attribute
onFocusSelection address action =
  on
    "click"
    (JD.succeed <| getSelection Nothing)
    (Signal.message address << action)

onClickControl : Signal.Address a -> a -> Html.Attribute
onClickControl address action =
  onWithOptions
    "click"
    { stopPropagation = False, preventDefault = False }
    JD.value
    (\_ -> Signal.message address action)
